# frozen_string_literal: true

Dir['./initializers/*.rb'].sort.each { |file| require file }

require 'byebug'
require 'dotenv/load'
require 'capybara'
require 'open-uri'
require 'nokogiri'

class HotmartDownloader
  def initialize
    @session = Capybara.current_session
    @last_course_length = 0
  end

  def execute
    login
    click_card_headers
  end

  private

  def login
    @session.visit('/login')
    @session.fill_in('login', with: ENV['EMAIL'])
    @session.fill_in('password', with: ENV['PASSWORD'])
    @session.find('button.btn-login').click
  end

  def click_card_headers
    @session.visit('/')
    @session.all('li.card.navigation-module').each do |section|
      @section_name = section.text.split.join(' ').gsub!('/', ' ')
      section.click
      click_video
    end
  end

  def click_video
    courses = @session.all('li.navigation-page.navigation-page-completed')
    courses[@last_course_length..].each do |course|
      @course_name = course.text
      course.click
      next unless @session.has_css?('iframe')

      @vimeo_url = @session.first('iframe')['src']
      scrapping_player_vimeo
      downloading_video
    end
    @last_course_length += courses[@last_course_length..].length - 1
  end

  def scrapping_player_vimeo
    page = Nokogiri::HTML(URI.open(@vimeo_url).read)
    data_vimeo_links = page.children.children.children.children.first(15).last
    select_resolution = select_resolution(data_vimeo_links)
    select_resolution = second_select_resolution(data_vimeo_links) unless select_resolution[0] == '"'
    select_resolution = third_select_resolution(data_vimeo_links) unless select_resolution[0] == '"'
    load_video_url(select_resolution)
  end

  def load_video_url(select_resolution)
    json_hd_video = JSON.parse(
      "{#{select_resolution}}"
    )
    @hd_video_url = json_hd_video['url']
  end

  def select_resolution(data_vimeo_links)
    fullhd = data_vimeo_links.to_s.scan(/.{311}1080p.{1}/)[2]
    hd = data_vimeo_links.to_s.scan(/.{311}720p.{1}/)[2]
    fullhd || hd
  end

  def second_select_resolution(data_vimeo_links)
    fullhd = data_vimeo_links.to_s.scan(/.{309}1080p.{1}/)[2]
    hd = data_vimeo_links.to_s.scan(/.{309}720p.{1}/)[2]
    fullhd || hd
  end

  def third_select_resolution(data_vimeo_links)
    fullhd = data_vimeo_links.to_s.scan(/.{307}1080p.{1}/)[2]
    hd = data_vimeo_links.to_s.scan(/.{307}720p.{1}/)[2]
    fullhd || hd
  end

  def downloading_video
    Dir.mkdir("videos/#{@section_name}") unless Dir.entries('./videos').include?(@section_name)
    puts "Downloading video #{@course_name} to folder videos/#{@section_name}"

    File.open("videos/#{@section_name}/#{@course_name}.mp4", 'w') do |f|
      f.write(URI.open(@hd_video_url).read)
    end

    puts 'Download finished!'
    puts ''
  end
end

HotmartDownloader.new.execute
