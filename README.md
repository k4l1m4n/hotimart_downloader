# hotimart_downloader

## How to use

create dotenv file
```
touch .env
```

edit the dotenv file to be like that, if the url is safe.club.hotmart then the 'safe' is the course name
```
COURSE_NAME='safe'
EMAIL='root@root.com'
PASSWORD='rootpassword'
```

then run the script
```
ruby ini.rb
```
