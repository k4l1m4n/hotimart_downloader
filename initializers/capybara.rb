# frozen_string_literal: true

require 'dotenv/load'
require 'capybara'
require 'webdrivers'

Capybara.configure do |config|
  config.default_driver = :selenium_chrome
  config.default_max_wait_time = 30
  # config.app_host = 'https://obootcamponebitcode.club.hotmart.com'
  config.app_host = "https://#{ENV['COURSE_NAME']}.club.hotmart.com"
end
